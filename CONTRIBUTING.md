Thanks for taking the time to contribute

Ensure the bug was not already reported by searching on GitLab under Issues.

If you're unable to find an open issue addressing the problem, open a new one. 
Its best to include a title and clear description, as much relevant information 
as possible, and a code sample or an executable test case demonstrating the 
expected behavior that is not occurring.