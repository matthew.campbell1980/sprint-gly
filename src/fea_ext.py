#!/usr/bin/env python
#coding=utf-8
# Feature extraction file  fea_ext.py

import sys
import string
import math
import os
import numpy
import re


# function
# define the dictionary with the phys properties for each AA
#1-Steric parameter,2-Polarizability,3-Volume (normalized van der Waals volume),4-Hydrophobicity,5-Isoelectric point,6-Helix probability,7-Sheet probability,8-polarity,9-Transfer energy, organic,solvent/water,10-Accessibility reduction Ratio,11-Net charge index of side chain,12-molecular weight,13-PK-N,14-PK-C,15-melting point,16-optical rotation,17-entropy of formation,18-heat capacity,19-absolute entropy, 20- Electronic charge index, 21- compared parameters related with hydrophobicity, 22- Compared parameter related with setric features, 23-Compared parameter related with electronic features, 24- levit probability of adopting alpha helix confirmation, 25- levit probability of adopting beta sheet confirmation (20, 21, 22, 23, 24, 25 from this paper "PROTDCAL: A Program to Compute General-Purpose Numerical Descriptors for Sequences and 3D-Structures of Proteins)

phys_dic = {'A': [ 0.305, 0.050, 0.124, 0.405, 0.406, 0.420, 0.230, 0.395, 0.147, 0.324, 0.000, 0.109, 0.585, 0.852, 0.717, 0.873, 0.124, 0.121, 0.141, 0.05, 0.07, -1.73, 0.09, 0.262, 1.29, 0.9],
            'C': [ 0.422, 0.130, 0.301, 0.782, 0.436, 0.170, 0.410, 0.074, 0.000, 0.210, 0.000, 0.357, 0.000, 0.164, 0.000, 0.691, 0.431, 0.592, 0.666, 0.15, 0.71, -0.97, 4.13, 0.353, 1.11, 0.74],
            'D': [ 0.382, 0.110, 0.344, 0.074, 0.000, 0.250, 0.200, 1.000, 0.000, 0.137, -1.000, 0.449, 0.546, 0.098, 0.554, 0.905, 0.314, 0.293, 0.364, 1.25, 3.64, 1.13, 2.36, 0.003, 1.04, 0.72],
            'E': [ 0.372, 0.150, 0.468, 0.113, 0.018, 0.420, 0.210, 0.914, 0.000, 0.256, -1.000, 0.558, 0.576, 0.459, 0.428, 0.974, 0.447, 0.398, 0.463, 1.36, 2.18, 0.53, -1.14, 0.071, 1.27, 0.8],
            'F': [ 0.702, 0.290, 0.729, 0.859, 0.349, 0.300, 0.380, 0.037, 0.735, 0.815, 0.000, 0.698, 0.362, 0.557, 0.639, 0.513, 0.361, 0.544, 0.602, 0.14, -4.92, 1.3, 0.45, 1, 1.07, 1.32],
            'G': [ 0.000, 0.000, 0.000, 0.310, 0.401, 0.130, 0.150, 0.506, 0.000, 0.227, 0.000, 0.000, 0.624, 0.869, 0.675, 0.855, 0.000, 0.000, 0.000, 0.02, 2.23, 5.36, 0.3, 0.012, 0.56, 0.92],
            'H': [ 0.714, 0.230, 0.577, 0.350, 0.608, 0.270, 0.300, 0.679, 0.147, 0.302, 0.000, 0.620, 0.358, 0.000, 0.596, 0.473, 0.538, 0.788, 0.944, 0.56, 2.41, 1.74, 1.11, 0.405, 1.22, 1.08],
            'I': [ 1.000, 0.190, 0.495, 0.862, 0.397, 0.300, 0.450, 0.037, 0.529, 1.000, 0.000, 0.434, 0.581, 0.885, 0.639, 0.978, 0.494, 0.467, 0.572, 0.09, -4.44, -1.68, -1.03, 0.768, 0.97, 1.45],
            'K': [ 0.451, 0.220, 0.590, 0.006, 0.904, 0.320, 0.270, 0.790, 0.000, 0.000, 1.000, 0.551, 0.362, 0.557, 0.277, 1.000, 0.810, 0.732, 0.881, 0.53, 2.84, 1.41, -3.14, 0.494, 1.23, 0.77],
            'L': [ 0.618, 0.190, 0.495, 0.831, 0.397, 0.390, 0.310, 0.000, 0.529, 0.693, 0.000, 0.434, 0.546, 0.885, 0.958, 0.746, 0.490, 0.533, 0.592, 0.01, -4.19, -1.03, -0.98, 0.795, 1.3, 1.02],
            'M': [ 0.561, 0.220, 0.548, 0.687, 0.354, 0.380, 0.320, 0.099, 0.382, 0.580, 0.000, 0.574, 0.376, 0.754, 0.633, 0.756, 0.351, 1.000, 0.700, 0.34,  -2.49, 0.27,  -0.41, 0.666, 1.47, 0.97],
            'N': [ 0.382, 0.130, 0.365, 0.126, 0.458, 0.210, 0.220, 0.827, 0.000, 0.056, 0.000, 0.442, 0.197, 0.328, 0.349, 0.800, 0.375, 0.320, 0.388, 1.31, 3.22, 1.45, 0.84, 0, 0.9, 0.76],
            'P': [ 0.637, 0.000, 0.337, 0.531, 0.494, 0.130, 0.340, 0.383, 0.000, 0.056, 0.000, 0.310, 1.000, 0.213, 0.265, 0.000, 0.244, 0.272, 0.331, 0.16, -1.22, 0.88, 2.23, 0.609, 0.52, 0.64],
            'Q': [ 0.372, 0.180, 0.489, 0.242, 0.347, 0.360, 0.250, 0.691, 0.000, 0.154, 0.000, 0.550, 0.341, 0.574, 0.042, 0.918, 0.505, 0.445, 0.501, 1.31, 3.08, 0.39, 0.07, 0.009, 1.44, 0.75],
            'R': [ 0.558, 0.290, 0.759, 0.000, 1.000, 0.360, 0.250, 0.691, 0.000, 0.125, 1.000, 0.767, 0.279, 0.000, 0.361, 0.979, 1.000, 0.058, 1.000, 1.69, 2.88, 2.52, -3.44, 0.204, 0.96,  0.99],
            'S': [ 0.313, 0.060, 0.198, 0.298, 0.353, 0.200, 0.280, 0.531, 0.000, 0.108, 0.000, 0.232, 0.376, 0.607, 0.301, 0.781, 0.217, 0.191, 0.250, 0.56, 1.96, -1.63, 0.57, 0.010, 0.82, 0.95],
            'T': [ 0.723, 0.110, 0.322, 0.390, 0.340, 0.210, 0.360, 0.457, 0.118, 0.137, 0.000, 0.341, 0.328, 0.443, 0.452, 0.577, 0.366, 0.252, 0.269, 0.65, 0.92, 2.09, -1.4, 0.242, 0.82, 1.21],
            'V': [ 0.876, 0.14, 0.371, 0.684, 0.394, 0.270, 0.490, 0.123, 0.441, 0.907, 0.000, 0.326, 0.555, 0.820, 0.693, 0.911, 0.374, 0.365, 0.412, 0.07, -2.69, -2.53, -1.29, 0.600, 0.91, 1.49],
            'W': [ 0.766, 0.41, 1.000, 1.000, 0.384, 0.320, 0.420, 0.062, 1.000, 0.756, 0.000, 1.000, 0.476, 1.000, 0.627, 0.521, 0.512, 0.728, 0.807, 1.08, -4.75, 3.65, 0.85, 0.94, 0.99, 1.14],
            'Y': [ 0.702, 0.3, 0.801, 0.604, 0.348, 0.250, 0.410, 0.160, 0.676, 0.210, 0.000, 0.822, 0.332, 0.623, 1.000, 0.756, 0.475, 0.614, 0.604, 0.72, -1.39, 2.32, 0.01,  0.666, 0.72, 1.25],
            'X': [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            'Z': [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]}


def compute_entropy(dis_list):
    """compute shannon entropy for a distribution.
    base = len(dis_list) is the base of log function 
    to make entropy between 0 and 1."""

    if sum(dis_list) == 0:
        return 0.0
    prob_list = map(lambda x:(x+0.0)/sum(dis_list),dis_list)
    ent = 0.0
    for prob in prob_list:
        if prob != 0:
            ent -= prob*math.log(prob,len(dis_list))
    return ent

def compute_ss_content(ss_seq_win):
    """compute ss content in a window."""
    con_C = con_H = con_E = 0
    for ss in ss_seq_win:
        if ss == 'C':
            con_C += 1
        elif ss == 'H':
            con_H += 1
        elif ss == 'E':
            con_E += 1
    act_len = con_C+con_H+con_E+0.0
    return ['%.3f'%(con_C/act_len),'%.3f'%(con_H/act_len),'%.3f'%(con_E/act_len)]

def compute_pcc(x,y):
    """compute the PCC between vector x and y"""
    mean_x = (sum(x)+0.0)/len(x)
    dev_x = sum([(i-mean_x)**2 for i in x])
    mean_y = (sum(y)+0.0)/len(y)
    dev_y = sum([(i-mean_y)**2 for i in y])
    if dev_x == 0 or dev_y == 0:
        return 0.0
    ret = 0.0
    for i in xrange(len(x)):
        ret += (x[i]-mean_x)*(y[i]-mean_y)
    return ret/math.sqrt(dev_x*dev_y)

def ss_to_num(sin_ss):
    """C->0,H->1,E->2,'$'->-1"""
    if sin_ss == 'C':
        return 0
    elif sin_ss == 'H':
        return 1
    elif sin_ss == 'E':
        return 2
    else:
        return -1

def tri_ss_to_num(tri_ss):
    """CCC->0,EEE->26,$XX->-1,XX$->-1
    return a 27-dimensional vector"""
    tri_num = []
    for ts in tri_ss:
        tri_num.append(ss_to_num(ts))
    num = -1
    # check for '$'
    if tri_num[0] != -1 and tri_num[2] != -1:
        num = 9*tri_num[0]+3*tri_num[1]+tri_num[2]
    ret = []
    for i in xrange(27):
        if i == num:
            ret.append('1')
        else:
            ret.append('0')
    return ret

def seg_bound(s_win):
    """Two boundaries of a segment"""
    c_ss = s_win[len(s_win)/2]
    l_len = r_len = 0
    i = len(s_win)/2 - 1
    while i >= 0:
        if s_win[i] != c_ss:
            break
        l_len += 1
        i -= 1
    i = len(s_win)/2 + 1
    while i < len(s_win):
        if s_win[i] != c_ss:
            break
        r_len += 1
        i += 1
    return (l_len,r_len)



# solvent accessibility for extented tripeptide (Ala-X-Ala).
ext_state = {'A':110.2,'D':144.1,'C':140.4,'E':174.7,'F':200.7,\
             'G':78.7,'H':181.9,'I':185.0,'K':205.7,'L':183.1,\
             'M':200.1,'N':146.4,'P':141.9,'Q':178.6,'R':229.0,\
             'S':117.2,'T':138.7,'V':153.7,'W':240.5,'Y':213.7,'X':999.0}


pid = sys.argv[1] 
win = 4 
base_path = 'PTM_Gly/' 
SPOT_out = base_path + 'Files/PSSM/Homo/' 
pssm_path = base_path + 'Files/PSSM/Homo/' 
fasta_path = base_path + 'Files/Fasta/Homo/' 
info_path = 'PTM_Gly/Server/misc/'
fea_path = 'PTM_Gly/Server/misc/'

#/*************************************************/
# build info file
# Read SPIDER file
fin = file(SPOT_out + pid + '.spd3','r')
spider = fin.readlines()[1:]
fin.close()
ss_res = map(lambda x:x.split()[1],spider)
ss_pre = map(lambda x:x.split()[2],spider)
ss_prob_c = map(lambda x:x.split()[8],spider)
ss_prob_h = map(lambda x:x.split()[10],spider)
ss_prob_e = map(lambda x:x.split()[9],spider)
asa_pre = map(lambda x:x.split()[3],spider)
rsa = []
for i in xrange(len(asa_pre)):
    rsa.append(float(asa_pre[i])/ext_state[ss_res[i]])


# Read HSE file
fin = file(SPOT_out + pid + '.hsa2','r')
HSE = fin.readlines()[1:]
fin.close()
HSE_res = map(lambda x:x.split()[1],HSE)
pre_CN = map(lambda x:x.split()[2],HSE)
pre_HSE_u = map(lambda x:x.split()[3],HSE)
pre_HSE_d = map(lambda x:x.split()[4],HSE)
pre_CN = [string.atof(x.strip(' ')) for x in pre_CN]
min_cn = min(pre_CN)
max_cn = max(pre_CN)
CN = [((q - min_cn)/(max_cn-min_cn)) for q in pre_CN]
pre_HSE_u = [string.atof(x.strip(' ')) for x in pre_HSE_u]
min_up = min(pre_HSE_u)
max_up = max(pre_HSE_u)
HSE_u = [((q - min_up)/(max_up-min_up)) for q in pre_HSE_u]
pre_HSE_d = [string.atof(x.strip(' ')) for x in pre_HSE_d]
min_dn = min(pre_HSE_d)
max_dn = max(pre_HSE_d)
HSE_d = [((q - min_dn)/(max_dn-min_dn)) for q in pre_HSE_d]


# read in pssm file
fin = file(pssm_path+pid+'.pssm','r')
pssm = fin.readlines()
fin.close()
if len(pssm[-6].split()) != 0 or pssm[3].split()[0] != '1': 
    print 'error on reading pssm, line -6 is not a spare line;\
     or line 3 is not the first line'
    sys.exit(1)
pssm = pssm[3:-6]

# read in fasta seq from fasta file
fin = file(fasta_path+pid+'.txt','r')
ann = fin.readlines()
fin.close()
ann_temp = ann[1:]
temp_line = [ann_temp1[:-1] for ann_temp1 in ann_temp]
ann[1] = (''.join(temp_line))
del ann[2:]
if len(ann) != 2:
    print 'check sequence',pid
    sys.exit(1)
fastaseq = ann[1].split()[0]

#find S and T amino acid positions in the sequence
O_index = 0
O_index = [m.start() for m in re.finditer('[TS]', fastaseq)]
if not O_index:
	print ' There is no O-linked(S/T) glycosylation amino acid in the sequence'


# check for seq in .ss2 and .spine
if not fastaseq == ''.join(HSE_res) == ''.join(ss_res):
	print 'Sequence inconsistent!',pid
	print 'fasta: ',fastaseq
	print '   ss: ',''.join(ss_res)
	print '  HSE: ',''.join(HSE_res)
	exit(1)

# Physicochemical
phys = [phys_dic[i] for i in ss_res]

SP = [str(i[0]) for i in phys]  
Pol = [str(i[1]) for i in phys] 
Vol = [str(i[2]) for i in phys] 
Hy = [str(i[3]) for i in phys]  
IP = [str(i[4]) for i in phys]  
HP = [str(i[5]) for i in phys]  
ShP = [str(i[6]) for i in phys] 
Plr = [str(i[7]) for i in phys] 
Sol = [str(i[8]) for i in phys] 
acs = [str(i[9]) for i in phys] 
chg = [str(i[10]) for i in phys] 
MW = [str(i[11]) for i in phys] 
PK_N = [str(i[12]) for i in phys]
PK_C = [str(i[13]) for i in phys]
MP = [str(i[14]) for i in phys]  
OR = [str(i[15]) for i in phys]  
EF = [str(i[16]) for i in phys]  
HC = [str(i[17]) for i in phys]  
AE = [str(i[18]) for i in phys]  
ECI = [str(i[19]) for i in phys] 
Z1 = [str(i[20]) for i in phys]  
Z2 = [str(i[21]) for i in phys]  
Z3 = [str(i[22]) for i in phys]  
Pa = [str(i[23]) for i in phys]  
Pb = [str(i[24]) for i in phys]  

# output 
fout = file(info_path+pid+'.info','w')
fout.write('>%s\n' %pid)
pos = 0
for i in xrange(len(fastaseq)):
    res = fastaseq[i]
    fout.write('%5d%5s%5s'%(i+1,res,res))
    if pssm[pos].split()[1] == res:
        for p_e in pssm[pos].split()[2:22]:
            fout.write(':%2s' %p_e)
        for p_e in pssm[pos].split()[22:42]:
            fout.write(':%3s' %p_e)
        fout.write(':%5s' %pssm[pos].split()[42])
    else:
        print 'Error reading pssm file!'
        flog = file(error_file,'a')
        flog.write(pid+': error on writing pssm, %s:%s\n' \
        %(pssm[pos].split()[1],res))
        flog.close()
        sys.exit(1)
    if ss_res[pos] == res:
        fout.write(':%s' %ss_pre[pos])
        fout.write(':%s' %ss_prob_c[pos])
        fout.write(':%s' %ss_prob_h[pos])
        fout.write(':%s' %ss_prob_e[pos])
    else:
        print 'Error reading ss file!'
        flog = file(error_file,'a')
        flog.write(pid+': error on writing ss, %s:%s\n' %(ss_res[pos],res))
        flog.close()
        sys.exit(1)
    if ss_res[pos] == res:
        fout.write(':%5.3f' %rsa[pos])
    else:
        print 'Error reading rsa file!'
        flog = file(error_file,'a')
        flog.write(pid+': error on writing rsa, %s:%s\n' %(rsa_res[pos],res))
        flog.close()
        sys.exit(1)
    pos += 1
    fout.write('\n')
fout.close()

#/*************************************************/
# build feature file
fin = file(info_path+'%s.info'%pid,'r')
info = fin.readlines()[1:]
fin.close()
fout = file(fea_path+'%s.fea'%pid,'w')
seq_len = len(info)
out_list = []
for i in xrange(len(info)):
    out_list.append([])


CN = map(str, CN)
HSE_u = map(str, HSE_u)
HSE_d = map(str, HSE_d)
for i in xrange(win):
    CN.insert(0,'1.000')
    CN.append('1.000')
for i in xrange(win,seq_len+win):
    for j in xrange(i-win,i+win+1):
        out_list[i-win].append(CN[j])
for i in xrange(win):
    HSE_u.insert(0,'1.000')
    HSE_u.append('1.000')
for i in xrange(win,seq_len+win):
    for j in xrange(i-win,i+win+1):
        out_list[i-win].append(HSE_u[j])
for i in xrange(win):
    HSE_d.insert(0,'1.000')
    HSE_d.append('1.000')
for i in xrange(win,seq_len+win):
    for j in xrange(i-win,i+win+1):
        out_list[i-win].append(HSE_d[j])

for i in xrange(win):
    SP.insert(0,'1.000')
    SP.append('1.000')
for i in xrange(win,seq_len+win):
    for j in xrange(i-win,i+win+1):
        if j != i:
            out_list[i-win].append(SP[j])
for i in xrange(win):
    Pol.insert(0,'1.000')
    Pol.append('1.000')
for i in xrange(win,seq_len+win):
    for j in xrange(i-win,i+win+1):
        if j != i:
            out_list[i-win].append(Pol[j])
for i in xrange(win):
    Vol.insert(0,'1.000')
    Vol.append('1.000')
for i in xrange(win,seq_len+win):
    for j in xrange(i-win,i+win+1):
        if j != i:
            out_list[i-win].append(Vol[j])
for i in xrange(win):
    Hy.insert(0,'1.000')
    Hy.append('1.000')
for i in xrange(win,seq_len+win):
    for j in xrange(i-win,i+win+1):
        if j != i:
            out_list[i-win].append(Hy[j])
for i in xrange(win):
    IP.insert(0,'1.000')
    IP.append('1.000')
for i in xrange(win,seq_len+win):
    for j in xrange(i-win,i+win+1):
        if j != i:
            out_list[i-win].append(IP[j])
for i in xrange(win):
    HP.insert(0,'1.000')
    HP.append('1.000')
for i in xrange(win,seq_len+win):
    for j in xrange(i-win,i+win+1):
        if j != i:
            out_list[i-win].append(HP[j])
for i in xrange(win):
    ShP.insert(0,'1.000')
    ShP.append('1.000')
for i in xrange(win,seq_len+win):
    for j in xrange(i-win,i+win+1):
        if j != i:
            out_list[i-win].append(ShP[j])
for i in xrange(win):
    Plr.insert(0,'1.000')
    Plr.append('1.000')
for i in xrange(win,seq_len+win):
    for j in xrange(i-win,i+win+1):
        if j != i:
            out_list[i-win].append(Plr[j])
for i in xrange(win):
    Sol.insert(0,'1.000')
    Sol.append('1.000')
for i in xrange(win,seq_len+win):
    for j in xrange(i-win,i+win+1):
        if j != i:
            out_list[i-win].append(Sol[j])
for i in xrange(win):
    acs.insert(0,'1.000')
    acs.append('1.000')
for i in xrange(win,seq_len+win):
    for j in xrange(i-win,i+win+1):
        if j != i:
            out_list[i-win].append(acs[j])
for i in xrange(win):
    chg.insert(0,'1.000')
    chg.append('1.000')
for i in xrange(win,seq_len+win):
    for j in xrange(i-win,i+win+1):
        if j != i:
            out_list[i-win].append(chg[j])
for i in xrange(win):
    MW.insert(0,'1.000')
    MW.append('1.000')
for i in xrange(win,seq_len+win):
    for j in xrange(i-win,i+win+1):
        if j != i:
            out_list[i-win].append(MW[j])
for i in xrange(win):
    PK_N.insert(0,'1.000')
    PK_N.append('1.000')
for i in xrange(win,seq_len+win):
    for j in xrange(i-win,i+win+1):
        if j != i:
            out_list[i-win].append(PK_N[j])
for i in xrange(win):
    PK_C.insert(0,'1.000')
    PK_C.append('1.000')
for i in xrange(win,seq_len+win):
    for j in xrange(i-win,i+win+1):
        if j != i:
            out_list[i-win].append(PK_C[j])
for i in xrange(win):
    MP.insert(0,'1.000')
    MP.append('1.000')
for i in xrange(win,seq_len+win):
    for j in xrange(i-win,i+win+1):
        if j != i:
            out_list[i-win].append(MP[j])
for i in xrange(win):
    OR.insert(0,'1.000')
    OR.append('1.000')
for i in xrange(win,seq_len+win):
    for j in xrange(i-win,i+win+1):
        if j != i:
            out_list[i-win].append(OR[j])
for i in xrange(win):
    EF.insert(0,'1.000')
    EF.append('1.000')
for i in xrange(win,seq_len+win):
    for j in xrange(i-win,i+win+1):
        if j != i:
            out_list[i-win].append(EF[j])
for i in xrange(win):
    HC.insert(0,'1.000')
    HC.append('1.000')
for i in xrange(win,seq_len+win):
    for j in xrange(i-win,i+win+1):
        if j != i:
            out_list[i-win].append(HC[j])
for i in xrange(win):
    AE.insert(0,'1.000')
    AE.append('1.000')
for i in xrange(win,seq_len+win):
    for j in xrange(i-win,i+win+1):
        if j != i:
            out_list[i-win].append(AE[j])
for i in xrange(win):
    ECI.insert(0,'1.000')
    ECI.append('1.000')
for i in xrange(win,seq_len+win):
    for j in xrange(i-win,i+win+1):
        if j != i:
            out_list[i-win].append(ECI[j])
for i in xrange(win):
    Z1.insert(0,'1.000')
    Z1.append('1.000')
for i in xrange(win,seq_len+win):
    for j in xrange(i-win,i+win+1):
        if j != i:
            out_list[i-win].append(Z1[j])
for i in xrange(win):
    Z2.insert(0,'1.000')
    Z2.append('1.000')
for i in xrange(win,seq_len+win):
    for j in xrange(i-win,i+win+1):
        if j != i:
            out_list[i-win].append(Z2[j])
for i in xrange(win):
    Z3.insert(0,'1.000')
    Z3.append('1.000')
for i in xrange(win,seq_len+win):
    for j in xrange(i-win,i+win+1):
        if j != i:
            out_list[i-win].append(Z3[j])
for i in xrange(win):
    Pa.insert(0,'1.000')
    Pa.append('1.000')
for i in xrange(win,seq_len+win):
    for j in xrange(i-win,i+win+1):
        if j != i:
            out_list[i-win].append(Pa[j])
for i in xrange(win):
    Pb.insert(0,'1.000')
    Pb.append('1.000')
for i in xrange(win,seq_len+win):
    for j in xrange(i-win,i+win+1):
        if j != i:
            out_list[i-win].append(Pb[j])

# write in pssm
pssm = map(lambda x:map(lambda y:'%7.5f' %(1/(1+math.pow(math.e,-string.atoi(y)))),x.split(':')[1:21]),info)
pssm_t = []
for i in xrange(20):
    pssm_t.append('%7.5f' %(1/(1+math.e**0)))
for i in xrange(win):
    pssm.insert(0,pssm_t)
    pssm.append(pssm_t)
for i in xrange(win,seq_len+win):
    for j in xrange(i-win,i+win+1):
        out_list[i-win].append(':'.join(pssm[j]))


wop = ['%7.5f' %compute_entropy(z) for z in map(lambda x:map(lambda y:string.atoi(y),x.split(':')[21:41]),info)]
for i in xrange(win):
    wop.insert(0,'%7.5f' %(0))
    wop.append('%7.5f' %(0))
for i in xrange(win,seq_len+win):
    for j in xrange(i-win,i+win+1):
        out_list[i-win].append(wop[j])

pssm = [[string.atoi(y) for y in x.split(':')[1:21]] for x in info]
pssm_t = []
for i in xrange(20):
    pssm_t.append(0)
for i in xrange(win):
    pssm.insert(0,pssm_t)
    pssm.append(pssm_t)
for i in xrange(win,seq_len+win):
    for j in xrange(i-win,i+win+1):
        if j != i:
            out_list[i-win].append('%.4f'%(compute_pcc(pssm[i],pssm[j])))

ss_prob = map(lambda x:x.split(':')[43:46],info)
for i in xrange(win):
    ss_prob.insert(0,['0.000','0.000','0.000'])
    ss_prob.append(['0.000','0.000','0.000'])
for i in xrange(win,seq_len+win):
    for j in xrange(i-win,i+win+1):
        out_list[i-win].append(':'.join(ss_prob[j]))

ss_seq = map(lambda x:x.split(':')[42],info)
for i in xrange(win):
    ss_seq.insert(0,'$')
    ss_seq.append('$')
for i in xrange(win,seq_len+win):
    out_list[i-win].append(':'.join(compute_ss_content(ss_seq[i-win:i+win+1])))

for i in xrange(win,seq_len+win):
    out_list[i-win].append(':'.join(tri_ss_to_num(ss_seq[i-1:i+2])))
    
for i in xrange(win,seq_len+win):
    [l_b,r_b] = seg_bound(ss_seq[i-win:i+win+1])
   
    for j in xrange(3):
        if j == ss_to_num(ss_seq[i]):
            out_list[i-win].append('%.3f'%((l_b+r_b+1.0)/(2*win+1)))
        else:
            out_list[i-win].append('0.000')

    for j in xrange(3):
        if j == ss_to_num(ss_seq[i]):
            out_list[i-win].append('%.3f'%((min(l_b,r_b)+0.0)/win))
        else:
            out_list[i-win].append('0.000')
    for j in xrange(3):
        if j == ss_to_num(ss_seq[i]):
            out_list[i-win].append('%.3f'%((max(l_b,r_b)+0.0)/win))
        else:
            out_list[i-win].append('0.000')

rsa = map(lambda x:x.split(':')[46].split()[0],info)
for i in xrange(win):
    rsa.insert(0,'1.000')
    rsa.append('1.000')
for i in xrange(win,seq_len+win):
    for j in xrange(i-win,i+win+1):
        out_list[i-win].append(rsa[j])

rsa = [string.atof(x.split(':')[46]) for x in info]
for i in xrange(win):
    rsa.insert(0,1.0)
    rsa.append(1.0)
for i in xrange(win,seq_len+win):
    for j in xrange(1,win+1):
        out_list[i-win].append('%.4f'%(sum(rsa[i-j:i+j+1])/(2*j+1)))


#Take features of S and T residues only
O_list = []
for i in xrange(len(O_index)):
    O_list.append([])
cnt = 0 
for x in O_index:
	O_list[cnt] = out_list[x]
	cnt += 1

# output to fea file
myList = []
for i in xrange(len(O_list)):
	fout.write('0')
	fout.write('\t')
	out_list1 = []
	out_list1.append(':'.join(O_list[i]))
	myList = [t.split(':') for t in out_list1]
	for k in myList:
		for p in enumerate(k):
			D = (p)
			fout.write(':'.join([str(D[0]+1),D[1]]))
			fout.write('\t')
	fout.write('\n')
fout.close()
