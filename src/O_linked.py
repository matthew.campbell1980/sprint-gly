# Prediction of lysine residues
outfile = file(fea_path+'%s.prob'%pid,'w')
sys.path.append ('') # Insert SVM library path
import svm
from svm import *
from svmutil import *

fname = open("type.txt", "r")
for f in fname:
    prot_type = f
    
prot_type = prot_type.strip()

if prot_type == "Mouse":
    m = svm_load_model('Mus_O_traindata.model')
elif prot_type == "Homo":
    m = svm_load_model('Homo_O_traindata.model')

a, b = svm_read_problem(fea_path+'%s.fea'%pid)
p_label, p_acc, p_val = svm_predict(a, b, m,'-b 1')
cnt = 0
cnt1 = len(fastaseq)
outfile.write("gly\tpos\tprob\ttype\n")
for item in p_val:
#append X residues if there is not enough upstream or down stream 
	if O_index[cnt] < win+1:
		gly_seg = fastaseq[0:O_index[cnt]+win+1]
		start = abs(O_index[cnt]-win)
		for p in xrange(start):
			gly_seg = 'X' + gly_seg
	elif O_index[cnt] >= len(fastaseq)-win:
		gly_seg = fastaseq[O_index[cnt]-win:len(fastaseq)]
		end = len(fastaseq) + (win - (len(fastaseq) - O_index[cnt]))
		while (cnt1 <= end):
			gly_seg = gly_seg + 'X'
			cnt1 += 1
#write in the file
	else:
		gly_seg = fastaseq[O_index[cnt]-win:O_index[cnt]+win+1]
#	print item[0]
#	os.system('read -s -n 1 -p "Press any key to continue..."')
	if item[0] <= 0.76:
		outfile.write("%s\t%s\t%2.3f\tnon-glycosite\n" % (gly_seg, str(O_index[cnt]+1), item[0]))
	else:
		outfile.write("%s\t%s\t%2.3f\tglyco-site\n" % (gly_seg, str(O_index[cnt]+1), item[0]))
	cnt += 1
outfile.write("* S and T amino acids with probability higher than 0.76 are cosidered as predicted glycosite")
outfile.close()

