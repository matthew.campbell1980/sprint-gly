%% Apply deeplearning on n_linked features

addpath(genpath('DeepLearnToolbox-master/DeepLearnToolbox-master'));


fea_file = importdata('fea.txt');
type = importdata('type.txt');

if strcmp(type(1,1), 'Homo')
    load('/models/Homo_N_traindata.mat');
else
    load('/models/Mus_N_traindata.mat');
end

prediction = nnff(nn, fea_file(:,2:end), fea_file(:,1));
dlmwrite('prob.txt', prediction.a{end},'precision','%.3f');
dlmwrite('prob.txt', [type{1,1}], '-append', 'delimiter','');
